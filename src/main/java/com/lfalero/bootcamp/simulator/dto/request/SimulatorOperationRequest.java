package com.lfalero.bootcamp.simulator.dto.request;

import lombok.Getter;
import lombok.Setter;
import javax.validation.constraints.*;
import java.math.BigDecimal;

@Getter
@Setter
public class SimulatorOperationRequest {

    @NotNull(message = "El DNI no debe ser nulo")
    @NotEmpty(message = "El DNI no debe ser vacio")
    @NotBlank(message = "El DNI no debe estar en blanco")
    @Pattern(regexp = "[\\s]*[0-9]*[1-9]+", message="El DNI solo deberá contener números")
    @Size(min = 8, max = 8, message = "El DNI deberá ser de 8 caracteres")
    private String dni;

    @NotNull(message = "La tarjeta no debe ser nulo")
    @NotEmpty(message = "La tarjeta no debe ser vacio")
    @NotBlank(message = "La tarjeta no debe estar en blanco")
    @Size(min = 3, max = 10)
    private String tarjeta;

    @NotNull(message = "La moneda no debe ser nulo")
    @NotEmpty(message = "La moneda no debe ser vacio")
    @NotBlank(message = "La moneda no debe estar en blanco")
    private String moneda;

    @NotNull(message = "El monto no debe ser nulo")
    private BigDecimal monto;

    @NotNull(message = "La cuota no debe ser nulo")
    @Min(value = 1, message = "La cuota debe ser mayor a 0")
    @Max(value = 36, message = "La cuota debe ser menor a 36")
    private Integer cuota;

    @NotNull(message = "La TEA no debe ser nulo")
    @NotEmpty(message = "La TEA no debe ser vacio")
    @NotBlank(message = "La TEA no debe estar en blanco")
    private String tea;

    @NotNull(message = "El día de pago no debe ser nulo")
    @Min(value = 5, message = "La cuota debe ser mayor a 5")
    @Max(value = 20, message = "La cuota debe ser menor a 20")
    private Integer diaPago;
}
