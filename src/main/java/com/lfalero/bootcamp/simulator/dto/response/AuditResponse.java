package com.lfalero.bootcamp.simulator.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
public class AuditResponse {

    private String dni;
    private String tarjeta;
    private String moneda;
    private BigDecimal monto;
    private Integer numeroCuota;
    private String tea;
    private Integer diaPago;
    private String primeraCuota;
    private BigDecimal cuota;
}
