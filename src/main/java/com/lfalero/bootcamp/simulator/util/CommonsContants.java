package com.lfalero.bootcamp.simulator.util;

import lombok.experimental.UtilityClass;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@UtilityClass
public class CommonsContants {

    public final String TL00001 = "TL00001";
    public final String TL00001_LITERAL = "GENERIC_ERROR";

    public final String TL00002 = "TL00002";
    public final String TL00002_LITERAL = "ARGUMENT_NOT_VALID";

    public final String TL00003 = "TL00003";
    public final String TL00003_LITERAL = "CARD_NOT_EXISTS";

    public final String TL00004 = "TL00004";
    public final String TL00004_LITERAL = "TEA_NOT_EXISTS";

    public final String ESTADO_OK = "exitoso";

    public final BigDecimal calculateTem(BigDecimal tea, BigDecimal dayPay) {
        BigDecimal newTea = tea.divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
        BigDecimal exponential = dayPay.divide(new BigDecimal(360), 2, RoundingMode.HALF_UP);

        BigDecimal formulaOne = BigDecimal.ONE.add(newTea);
        BigDecimal formulaTwo = new BigDecimal(Math.pow(
                formulaOne.doubleValue(),
                exponential.doubleValue())
        );
        BigDecimal formulaThree = formulaTwo.subtract(BigDecimal.ONE);
        return formulaThree
                .multiply(new BigDecimal(100))
                .setScale(2, RoundingMode.HALF_UP);
    }

    public final BigDecimal calculateQuota(BigDecimal amount, BigDecimal tem, Integer numberQuotas) {
        BigDecimal formulaOne = BigDecimal.ONE.add(tem);
        BigDecimal formulaTwo = formulaOne.pow(numberQuotas);
        BigDecimal formulaThree = tem.multiply(formulaTwo);
        BigDecimal formulaFour = formulaTwo.subtract(BigDecimal.ONE);
        BigDecimal formulaFive = formulaThree.divide(formulaFour,  2, RoundingMode.HALF_UP);
        return amount
                .multiply(formulaFive)
                .setScale(2, RoundingMode.HALF_UP);
    }

    public final String calculateFirstQuota(Integer payDay) {
        DateTimeFormatter esDateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate now = LocalDate.now().withDayOfMonth(payDay).plusMonths(1);
        return now.format(esDateFormat);
    }
}
