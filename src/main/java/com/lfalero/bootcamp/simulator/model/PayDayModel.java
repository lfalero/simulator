package com.lfalero.bootcamp.simulator.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "pay_day")
public class PayDayModel extends BaseModel{

    private Integer value;
}
