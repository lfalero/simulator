package com.lfalero.bootcamp.simulator.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
@Table(name = "tea")
public class TeaModel extends BaseModel{

    private BigDecimal value;
}
