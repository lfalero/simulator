package com.lfalero.bootcamp.simulator.rest;

import com.lfalero.bootcamp.simulator.dto.request.SimulatorOperationRequest;
import com.lfalero.bootcamp.simulator.dto.response.AuditResponse;
import com.lfalero.bootcamp.simulator.dto.response.DetailInformationResponse;
import com.lfalero.bootcamp.simulator.dto.response.SimulatorOperationResponse;
import com.lfalero.bootcamp.simulator.service.SimulatorService;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@Validated
@RestController
@RequestMapping("v1/simulator")
@ControllerAdvice
@AllArgsConstructor
public class SimulatorController {

    private final SimulatorService simulatorService;

    @GetMapping(value = "/detail")
    public Maybe<DetailInformationResponse> detailInformation() {
        return simulatorService.detailInformation();
    }

    @PostMapping(value = "/operation")
    public Maybe<SimulatorOperationResponse> simulatorOperation(@Valid @RequestBody SimulatorOperationRequest request) {
        return simulatorService.simulatorOperation(request);
    }

    @GetMapping(value = "/audit")
    public Flowable<AuditResponse> auditOperation() {
        return simulatorService.auditOperation();
    }

}
