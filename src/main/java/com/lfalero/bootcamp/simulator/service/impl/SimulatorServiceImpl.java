package com.lfalero.bootcamp.simulator.service.impl;

import com.lfalero.bootcamp.simulator.dto.request.SimulatorOperationRequest;
import com.lfalero.bootcamp.simulator.dto.response.AuditResponse;
import com.lfalero.bootcamp.simulator.dto.response.DetailInformationResponse;
import com.lfalero.bootcamp.simulator.dto.response.SimulatorOperationResponse;
import com.lfalero.bootcamp.simulator.exception.ServiceException;
import com.lfalero.bootcamp.simulator.exception.ServiceGenericException;
import com.lfalero.bootcamp.simulator.model.AuditModel;
import com.lfalero.bootcamp.simulator.model.CardModel;
import com.lfalero.bootcamp.simulator.model.QuotaModel;
import com.lfalero.bootcamp.simulator.model.TeaModel;
import com.lfalero.bootcamp.simulator.repository.*;
import com.lfalero.bootcamp.simulator.service.SimulatorService;
import com.lfalero.bootcamp.simulator.util.CommonsContants;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.schedulers.Schedulers;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


@Slf4j
@AllArgsConstructor
@Service
public class SimulatorServiceImpl implements SimulatorService {

    private final CardRepository cardRepository;
    private final PayDayRepository payDayRepository;
    private final QuotaRepository quotaRepository;
    private final TeaRepository teaRepository;
    private final AuditRepository auditRepository;

    @Override
    public Maybe<DetailInformationResponse> detailInformation() {
        return Maybe
            .fromCallable(() -> {

                List<String> responseCards = cardRepository.findAll()
                    .stream()
                    .map(element -> element.getValue())
                    .collect(Collectors.toList());

                QuotaModel quotaModel = quotaRepository.getById(1L);
                List<Integer> responseQuotas = IntStream
                    .range(quotaModel.getMin(), quotaModel.getMax()+1)
                    .boxed()
                    .collect(Collectors.toList());

                List<String> responseTeas = teaRepository.findAll()
                    .stream()
                    .map(element ->  {
                        String tea = element.getValue()
                            .setScale(2, RoundingMode.HALF_UP)
                            .toString().concat("%");
                        return tea;
                    })
                    .collect(Collectors.toList());

                List<Integer> responsePayDay = payDayRepository.findAll()
                    .stream()
                    .map(element -> element.getValue())
                    .collect(Collectors.toList());

                DetailInformationResponse response = new DetailInformationResponse();
                response.setTarjetas(responseCards);
                response.setCuotas(responseQuotas);
                response.setTea(responseTeas);
                response.setDiaPagos(responsePayDay);
                return  response;
            })
            .subscribeOn(Schedulers.io())
            .doOnSuccess(element -> log.info(element.toString()))
            .onErrorResumeNext(element -> {
                return Maybe.error(
                    new ServiceGenericException(
                        CommonsContants.TL00001,
                        CommonsContants.TL00001_LITERAL,
                        element.getMessage()
                    )
                );
            });
    }

    @Override
    public Maybe<SimulatorOperationResponse> simulatorOperation(SimulatorOperationRequest request) {
        return Maybe
            .fromCallable(() -> {
                BigDecimal tea =  new BigDecimal(request.getTea().substring(0, request.getTea().length()-1));
                BigDecimal dayPay = BigDecimal.valueOf(request.getDiaPago());
                BigDecimal tem = CommonsContants.calculateTem(tea, dayPay);
                BigDecimal quota = CommonsContants.calculateQuota(request.getMonto(), tem, request.getCuota());

                SimulatorOperationResponse response = new SimulatorOperationResponse();
                response.setCuota(quota);
                response.setMoneda(request.getMoneda());
                response.setPrimeraCuota(CommonsContants.calculateFirstQuota(request.getDiaPago()));
                response.setEstado(CommonsContants.ESTADO_OK);
                return response;
            })
            .flatMap(element -> {
                CardModel cardModel = cardRepository.findByValue(request.getTarjeta());
                if (cardModel != null) {
                    return Maybe.just(element);
                } else {
                    return Maybe.error(new InterruptedException(CommonsContants.TL00003));
                }
            })
            .flatMap(element -> {
                BigDecimal tea =  new BigDecimal(request.getTea().substring(0, request.getTea().length()-1));
                TeaModel teaModel = teaRepository.findByValue(tea);
                if (teaModel != null) {
                    return Maybe.just(element);
                } else {
                    return Maybe.error(new InterruptedException(CommonsContants.TL00004));
                }
            })
            .subscribeOn(Schedulers.io())
            .doOnSuccess(element -> {
                log.info(element.toString());

                AuditModel auditModel = new AuditModel();
                auditModel.setDni(request.getDni());
                auditModel.setCard(request.getTarjeta());
                auditModel.setMoney(request.getMoneda());
                auditModel.setAmount(request.getMonto());
                auditModel.setNumberQuota(request.getCuota());
                auditModel.setTea(request.getTea());
                auditModel.setPayDay(request.getDiaPago());
                auditModel.setFirstQuota(element.getPrimeraCuota());
                auditModel.setQuota(element.getCuota());
                auditRepository.save(auditModel);
            })
            .onErrorResumeNext(element -> {
                if(element.getMessage().equalsIgnoreCase(CommonsContants.TL00003)) {
                    return Maybe.error(
                        new ServiceException(
                            CommonsContants.TL00003,
                            CommonsContants.TL00003_LITERAL,
                            HttpStatus.BAD_REQUEST
                        )
                    );
                } else if(element.getMessage().equalsIgnoreCase(CommonsContants.TL00004)) {
                    return Maybe.error(
                        new ServiceException(
                            CommonsContants.TL00004,
                            CommonsContants.TL00004_LITERAL,
                            HttpStatus.BAD_REQUEST
                        )
                    );
                } else {
                    return Maybe.error(
                        new ServiceGenericException(
                            CommonsContants.TL00001,
                            CommonsContants.TL00001_LITERAL,
                            element.getMessage()
                        )
                    );
                }
            });
    }

    @Override
    public Flowable<AuditResponse> auditOperation() {
        return Flowable
            .fromIterable(auditRepository.findAll())
                .map(element -> {
                    AuditResponse auditResponse = new AuditResponse();
                    auditResponse.setDni(element.getDni());
                    auditResponse.setTarjeta(element.getCard());
                    auditResponse.setMoneda(element.getMoney());
                    auditResponse.setMonto(element.getAmount());
                    auditResponse.setNumeroCuota(element.getNumberQuota());
                    auditResponse.setTea(element.getTea());
                    auditResponse.setDiaPago(element.getPayDay());
                    auditResponse.setPrimeraCuota(element.getFirstQuota());
                    auditResponse.setCuota(element.getQuota());
                    return auditResponse;
                })
                .subscribeOn(Schedulers.io())
                .doOnNext(element -> log.info(element.toString()))
                .onErrorResumeNext(element -> {
                    return Flowable.error(
                        new ServiceGenericException(
                            CommonsContants.TL00001,
                            CommonsContants.TL00001_LITERAL,
                            element.getMessage()
                        )
                    );
                });
    }
}
