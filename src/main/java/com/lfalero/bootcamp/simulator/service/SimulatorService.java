package com.lfalero.bootcamp.simulator.service;

import com.lfalero.bootcamp.simulator.dto.request.SimulatorOperationRequest;
import com.lfalero.bootcamp.simulator.dto.response.AuditResponse;
import com.lfalero.bootcamp.simulator.dto.response.DetailInformationResponse;
import com.lfalero.bootcamp.simulator.dto.response.SimulatorOperationResponse;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

public interface SimulatorService {

    Maybe<DetailInformationResponse> detailInformation();
    Maybe<SimulatorOperationResponse> simulatorOperation(SimulatorOperationRequest request);
    Flowable<AuditResponse> auditOperation();
}
