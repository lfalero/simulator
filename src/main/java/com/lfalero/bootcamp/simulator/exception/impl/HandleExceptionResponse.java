package com.lfalero.bootcamp.simulator.exception.impl;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class HandleExceptionResponse {

    private String code;
    private String literal;

}
