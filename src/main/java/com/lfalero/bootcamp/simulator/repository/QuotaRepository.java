package com.lfalero.bootcamp.simulator.repository;

import com.lfalero.bootcamp.simulator.model.QuotaModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface QuotaRepository extends CrudRepository<QuotaModel, Long> {

    QuotaModel getById(Long id);

}
