INSERT INTO card(id, value, created_at) VALUES (1, 'Clasica', '2021-04-03 10:00:00');
INSERT INTO card(id, value, created_at) VALUES (2, 'Oro', '2021-04-03 10:00:00');
INSERT INTO card(id, value, created_at) VALUES (3, 'Black', '2021-04-03 10:00:00');

INSERT INTO quota(id, min, max, created_at) VALUES (1, 1, 36, '2021-04-03 10:00:00');

INSERT INTO tea(id, value, created_at) VALUES (1, 99.90, '2021-04-03 10:00:00');
INSERT INTO tea(id, value, created_at) VALUES (2, 95.90, '2021-04-03 10:00:00');
INSERT INTO tea(id, value, created_at) VALUES (3, 90.90, '2021-04-03 10:00:00');

INSERT INTO pay_day(id, value, created_at) VALUES (1, 5, '2021-04-03 10:00:00');
INSERT INTO pay_day(id, value, created_at) VALUES (2, 20, '2021-04-03 10:00:00');